
function Number(num) {

    arr = [ [1, 2, 2, 0, 2, 2, 1],
            [0, 0, 2, 0, 0, 2, 0],
            [1, 0, 2, 1, 2, 0, 1],
            [1, 0, 2, 1, 0, 2, 1],
            [0, 2, 2, 1, 0, 2, 0],
            [1, 2, 0, 1, 0, 2, 1],
            [1, 2, 0, 1, 2, 2, 1],
            [1, 0, 2, 0, 0, 2, 0],
            [1, 2, 2, 1, 2, 2, 1],
            [1, 2, 2, 1, 0, 2, 1],
          ];

    return arr[num];
}

function inputHandle(str) {

    if(str === null || str === undefined || str === '') throw new Error('参数输入错误');

    /* let reg = /\D/;
    if(reg.test(str)) throw new Error('参数输入错误'); */
    
    let input = [];
    for(let i = 0; i < str.length; i++) {
        input[i] = Number(parseInt(str[i]));
    }
    return input;
}

function joinAll(input, size) {
    for(let i = 0; i < input.length; i++) {
        input[i] = scale(input[i], size);
    }
    let inputJoin = input[0];
    for(let i = 1; i < input.length; i++) {
        inputJoin = joinTwoArr(inputJoin, input[i]);
    }
    return inputJoin;
}

function joinTwoArr(arr1, arr2) {
    for(let i = 0; i< arr1.length; i++) {
        for(let j = 0; j < arr2[0].length; j++) {
            arr1[i].push(arr2[i][j]);
        }
    }
    return arr1;
}

function scale(arr, size = 1) {
    
    let newLine = 2 * size + 3;
    let newCol = size + 2;
    let newArr = new Array(newLine);
    for(let i =  0; i < newArr.length; i++) {
        newArr[i] = new Array(newCol);
        for(let j = 0; j < newCol; j++) {
            newArr[i][j] = 0;
        }
    }

    if(arr[0] === 1) {
        for(let i = 1; i <= size; i++) {
            newArr[0][i] = 1;
        }
    }

    if(arr[3] === 1) {
        for(let i = 1; i <= size; i++) {
            newArr[parseInt(newLine / 2)][i] = 1;
        }
    }

    if(arr[6] === 1) {
        for(let i = 1; i <= size; i++) {
            newArr[newLine - 1][i] = 1;
        }
    }

    if(arr[1] === 2) {
        for(let i = 1; i <= size; i++) {
            newArr[i][0] = 2;
        }
    }

    if(arr[4] === 2) {
        for(let i = 1; i <= size; i++) {
            newArr[parseInt(newLine / 2) + i][0] = 2;
        }
    }

    if(arr[2] === 2) {
        for(let i = 1; i <= size; i++) {
            newArr[i][newCol - 1] = 2;
        }
    }

    if(arr[5] === 2) {
        for(let i = 1; i <= size; i++) {
            newArr[parseInt(newLine / 2) + i][newCol - 1] = 2;
        }
    }
    
    return newArr;

}

function printNum(arr) {
    let result = '';
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[0].length; j++) {
            if(arr[i][j] === 0) {
                result += ' ';
            }
            else if(arr[i][j] === 1) {
                result += '-';
            }
            else {
                result += '|';
            }
        }
        result = result + '\n';   
    }
    return result;
}



function test(input, size) {

    let handle_input = inputHandle(input);
    let number_print = joinAll(handle_input, size);
    console.log(printNum(number_print));

}

let input = '0123456789';
let size = 2;
test(input, size);


